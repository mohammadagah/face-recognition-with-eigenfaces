﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using Accord.Statistics.Analysis;
using Accord.Imaging.Converters;
using Accord.Imaging.Filters;

using System.IO;

namespace FaceReconigtion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Bitmap> EigenFaces = new System.Collections.Generic.List<Bitmap>();
        
        public static Bitmap MakeGrayscale3(Bitmap original)  // change a mapcolor image to a grayscale image
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            //create the grayscale ColorMatrix
            ColorMatrix colorMatrix = new ColorMatrix(
               new float[][] 
      {
         new float[] {.3f, .3f, .3f, 0, 0},
         new float[] {.59f, .59f, .59f, 0, 0},
         new float[] {.11f, .11f, .11f, 0, 0},
         new float[] {0, 0, 0, 1, 0},
         new float[] {0, 0, 0, 0, 1}
         
      });

            //create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            //set the color matrix attribute
            attributes.SetColorMatrix(colorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();
            return newBitmap;
        }
        private int[] ImgToVector(Bitmap img)
        {
            img = MakeGrayscale3(img);
            int n = img.Height*img.Width ;
            int[] res =  new int[n] ;
            int i = 0;
            for (int y = 0; y < img.Height; y++)
                for (int x = 0; x < img.Width; x++)
                {
                    res[i] = img.GetPixel(x, y).R;// for a grayscale img r & g & b are the same
                    i++;
                }
            return res;
        }
        private Bitmap dec_size(Bitmap img)
        {
            Bitmap newimg = new Bitmap(img ,new Size(128,128));
            return newimg;
        }
        
        private int[,] getMatrix( List<int[]> vectorList) 
        {
            int vec_size = 16384;
            int[,] matrix = new int[vectorList.Count,vec_size];
            for (int i = 0; i < vectorList.Count ; i++)
                for(int j = 0; j < vec_size ; j++)
                {
                    matrix[i,j] = vectorList[i][j];
                }
            return matrix;
        }

        private int[] aveVec(int[,] matrix , Size size)
        {
            int[] result = new int[size.Height];
            for (int j = 0; j < size.Height; j++)
            {
                int sum = 0;
                for (int i = 0; i < size.Width; i++)
                {
                    sum += matrix[i, j];
                }
                int ave = Convert.ToInt32(sum / (size.Width));
                result[j] = ave;
            }
            return result;
        }

        private int[,] centralize(int[,] matrix , int[] aveVec ,Size size)
        {
            int[,] resMatrix = new int[size.Width, size.Height];
            for (int i = 0 ; i < size.Width ; i++)
                for (int j = 0; j < size.Height; j++)
                {
                    resMatrix[i, j] = matrix[i, j] - aveVec[j];
                }
            return resMatrix;
        }

        private int[,] transpose(int[,] matrix, Size size)
        {
            int[,] transposed = new  int[size.Height,size.Width];
            for (int i = 0 ; i < size.Width ; i++)
                for (int j = 0; j < size.Height ;j++)
                {
                    transposed[j, i] = matrix[i, j];
                }
            return transposed;
        }

        private double [,] multiplyMatrixes(double [,] A, double [,] B , Size asize , Size bsize)
        {
            double[,] result = new double[bsize.Width,asize.Height];
            for(int i  = 0; i < asize.Height ; i++)
                for (int j = 0; j < bsize.Width ; j++)
                {
                    double sum = 0;
                    for (int k = 0 ; k < asize.Width ; k++)
                    {
                        sum += A[k,i]*B[j,k];
                    }
                    result[j,i] = sum;
                }
            return result;
        }

        private int[,] covarianse(int[,] matrix , Size size)  // return covariance of a matrix
        {
            int N = size.Width;
            int M = size.Height;
            int[,] c = new int[size.Height, size.Height];
            int[,] transposedMatrix = transpose(matrix, size);
            //c = multiplyMatrixes(transposedMatrix, matrix, new Size(M, N), size);
            for(int i = 0; i < M;i++)
                for (int j = 0; j < M; j++)
                {
                    c[i, j] /= N;
                }
            return c;
        }
        private void putImage(double [] vec)
        {
            Bitmap flag = new Bitmap(128, 128);
            Graphics flagGraphics = Graphics.FromImage(flag);
            int k = 0;
            for (int i = 0; i < 128; i++)
                for (int j = 0; j < 128; j++)
                {
                    int c = Convert.ToInt32(vec[k]);
                    flag.SetPixel(i, j, Color.FromArgb(c, c, c));
                    k++;
                }
                
        }
        private int det(int[,] matrix , Size size)   // return determinant of a matrix
        {
            int res = 0;
            int N = size.Width;
            if (N == 1)
            {
                return matrix[0, 0];
            }
            else
            {
                for (int i = 0; i < N; i++)
                {
                    int spend_i = 0;
                    int[,] temp = new int[N-1, N-1];
                    for (int j = 0; j < N; j++)
                    {
                        if (j != i)
                        {
                            for (int k = 1; k < N; k++)
                            {
                                temp[j-spend_i, k - 1] = matrix[j , k];
                            }
                            
                        }
                        else { spend_i = 1 ; }
                    }
                    int sgn = -1;
                    if (i % 2 == 0) { sgn = +1; }
                    res += det(temp, new Size(N-1,N-1)) * sgn *matrix[i,0];
                }
            }
            return res;
        }

        private PrincipalComponentAnalysis  doPCA(double [,] covariance) //This function has a bug and I will debug it later
        {
            var pca = new PrincipalComponentAnalysis(covariance, AnalysisMethod.Center);
            pca.Compute();
            return pca;
            //double[,] components = pca.Transform(covariance,2);
            //return components;
        }
        private double[,] matrixToDouble(int[,] A , Size Asize)
        {
            double[,] newA = new double[Asize.Width, Asize.Height];
            for(int i = 0; i < Asize.Width ; i++)
                for (int j = 0; j < Asize.Height; j++)
                {
                    newA[i, j] = A[i, j];
                }
            return newA;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EigenFaces.Clear();
            for (int i = 0; i < 200; i++)
            {
                Bitmap eigenface = new Bitmap("Eigenfaces//EF" + i.ToString() + ".bmp");
                EigenFaces.Add(eigenface);
            }
            numeric.Maximum = EigenFaces.Count - 1;
        }

        private double EuclideanDistance(double [] a, double [] b)
        {
            double sumOfDistanses = 0;
            for (int i = 0; i < a.Length; i++)
            {
                sumOfDistanses += Math.Abs(a[i] - b[i]);
            }
            return sumOfDistanses;
        }

        private double SimWithPCA(Bitmap A , Bitmap B)
        {
            string[] allImageNames = Directory.GetFiles("C:\\Users\\m.agah\\Desktop\\face rec\\JPG", "*.jpg")
                                         .Select(path => Path.GetFileName(path))
                                         .ToArray();

            string[] imageNames = new string[40];
            for (int t = 0; t < 40; t++) { imageNames[t] = allImageNames[t]; }

            int imagecount = imageNames.Count();
            int imagesize = 16384;
            int[,] imagesMatrix = new int[imagesize, imagecount];
            int imgid = 0;
            foreach (string fn in imageNames)
            {
                Bitmap img = new Bitmap("C:\\Users\\m.agah\\Desktop\\face rec\\JPG\\" + fn);
                img = MakeGrayscale3(img);
                img = dec_size(img);
                int[] vec = ImgToVector(img);
                for (int k = 0; k < imagesize; k++)
                {
                    imagesMatrix[k, imgid] = vec[k];
                }
                imgid++;
            }
            double[,] data = matrixToDouble(imagesMatrix, new Size(imagesize, imagecount));
            double[] mean = Accord.Statistics.Tools.Mean(data);
            double[,] cov = Accord.Statistics.Tools.Covariance(data, mean);
            //int[,] cov = covarianse(imagesMatrix, new Size(imagecount, imagesize));
            //double[,] doubleCov = matrixToDouble(cov , new Size(imagesize,imagesize));
            PrincipalComponentAnalysis X = PrincipalComponentAnalysis.FromCovarianceMatrix(mean, cov);
            X.Compute();
            double[,] newSpace = multiplyMatrixes(X.ComponentMatrix, X.Transform(data), new Size(40, 40), new Size(16384,40));
            // get some of eigen vectors
            double[] EigenValues = new double[imagecount];
            for (int i = 0; i < imagecount; i++) { EigenValues[i] = X.ComponentMatrix[i, i]; }
            return 0;
        }

        private double VectorSize(int[] A)
        {
            double res = 0;
            for (int i = 0; i < A.Length; i++)
            {
                res += A[i] * A[i];
            }
            return Math.Sqrt(res);
        }
        private double VectorSize(double[] A)
        {
            double res = 0;
            for (int i = 0; i < A.Length; i++)
            {
                res += A[i] * A[i];
            }
            return Math.Sqrt(res);
        }

        private double Project(int[] a, int[] b)
        {
            double res = 0;
            for (int i = 0; i < a.Length; i++)
            {
                res += a[i] * b[i];
            }
            return res / (VectorSize(b));
        }


        private double Project(double[] a, double[] b)
        {
            double res = 0;
            for (int i = 0; i < a.Length; i++)
            {
                res += a[i] * b[i];
            }
            return res / (VectorSize(b));
        }
        double SimRate(double[] A, double[] B)
        {
            int imagecount = EigenFaces.Count;
            double simrate = 0;
            for (int k = 0; k < imagecount; k++)
            {
                simrate += A[k] * B[k];
            }
            simrate /= VectorSize(A) * VectorSize(B);
            simrate *= 1000;
            int simrateint = Convert.ToInt32(Math.Floor(simrate));
            simrate -= simrateint;
            return simrate;
        }
        private double Similarity(Bitmap A , Bitmap B)
        {
            // get names of all image in fa subset
            string[] allImageNames = Directory.GetFiles("C:\\Users\\m.agah\\Desktop\\face rec\\JPG", "*.jpg")
                                         .Select(path => Path.GetFileName(path))
                                         .ToArray();

            string[] imageNames = new string[100]; // choose a part of them
            for (int t = 0; t < 100; t++) { imageNames[t] = allImageNames[t]; }

            int imagecount = imageNames.Count();
            int imagesize = 16384; // 128 * 128
            int[,] imagesMatrix = new int[imagesize, imagecount];   // matrix of library images
            List<int[]> ImageSet = new System.Collections.Generic.List<int[]>();

            int imgid = 0;
            foreach (string fn in imageNames)
            {
                Bitmap img = new Bitmap("C:\\Users\\m.agah\\Desktop\\face rec\\JPG\\" + fn);
                img = MakeGrayscale3(img);
                img = dec_size(img);
                int[] vec = ImgToVector(img);
                ImageSet.Add(vec);
                for (int k = 0; k < imagesize; k++)
                {
                    imagesMatrix[k, imgid] = vec[k];
                }
                imgid++;
            }
            A = MakeGrayscale3(A);
            A = dec_size(A);
            B = MakeGrayscale3(B);
            B = dec_size(B);
            int[] a = ImgToVector(A);
            double[] wa = new double [imagecount];
            int[] b = ImgToVector(B);
            double[] wb = new double [imagecount];
            for (int i = 0; i < imagecount; i++)
            {
                wa[i] = Project(a, ImageSet[i]);
                wb[i] = Project(b, ImageSet[i]);
            }
            double simrate = 0;
            for (int k = 0; k < imagecount; k++)
            {
                simrate += wa[k] * wb[k];
            }
            simrate /= VectorSize(wa) * VectorSize(wb);
            simrate *= 1000;
            int simrateint = Convert.ToInt32(Math.Floor(simrate));
            simrate -= simrateint;
            Console.WriteLine(simrate);
            return EuclideanDistance(wa, wb);

        }

        double SimilarityWithPCA(Bitmap A , Bitmap B)
        {
            List<int[]> imageSet = new System.Collections.Generic.List<int[]>();
            for (int i = 0; i < EigenFaces.Count; i++)
            {
                imageSet.Add(ImgToVector(EigenFaces[i]));
            }
            A = MakeGrayscale3(A);
            A = dec_size(A);
            B = MakeGrayscale3(B);
            B = dec_size(B);
            int[] a = ImgToVector(A);
            int imagecount = EigenFaces.Count;
            double[] wa = new double[imagecount];
            int[] b = ImgToVector(B);
            double[] wb = new double[imagecount];
            for (int i = 0; i < imagecount; i++)
            {
                wa[i] = Project(a, imageSet[i]);
                wb[i] = Project(b, imageSet[i]);
            }

            Console.WriteLine(SimRate(wa,wb));
            return EuclideanDistance(wa, wb);

        }

        private PrincipalComponentAnalysis test()
        {
            Random r = new Random();
            int M = 50;
            double[,] A = new double[M, M];
            for(int i = 0; i < M ; i++)
                for (int j = 0; j < M; j++)
                {
                    A[i, j] = r.Next(0, 256);
                }
            PrincipalComponentAnalysis pca = new PrincipalComponentAnalysis(A);
            pca.Compute();
            return pca;
        }

        private bool check(double[,] A, Size size)
        {
            for(int i = 0 ; i < size.Width ; i++)
                for (int j = 0; j < size.Height; j++)
                {
                    if (A[i, j] > 256) { return false; }
                }
            return true;
        }
        public string Aname = "";
        public string Bname = "";
        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Aname = openFileDialog1.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Bname = openFileDialog1.FileName;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Bitmap picA = new Bitmap(Aname);
            Bitmap picB = new Bitmap(Bname);
            double limit = 60000;
            if(Similarity(picA, picB) <=  limit ){Console.WriteLine("TRUE");}
            else{Console.WriteLine("FALSE");}
            
        }


        public static Bitmap ColorToGrayscale(Bitmap bmp)
        {
            int w = bmp.Width,
            h = bmp.Height,
            r, ic, oc, bmpStride, outputStride, bytesPerPixel;
            PixelFormat pfIn = bmp.PixelFormat;
            ColorPalette palette;
            Bitmap output;
            BitmapData bmpData, outputData;

            //Create the new bitmap
            output = new Bitmap(w, h, PixelFormat.Format8bppIndexed);

            //Build a grayscale color Palette
            palette = output.Palette;
            for (int i = 0; i < 256; i++)
            {
                Color tmp = Color.FromArgb(255, i, i, i);
                palette.Entries[i] = Color.FromArgb(255, i, i, i);
            }
            output.Palette = palette;

            //No need to convert formats if already in 8 bit
            if (pfIn == PixelFormat.Format8bppIndexed)
            {
                output = (Bitmap)bmp.Clone();

                //Make sure the palette is a grayscale palette and not some other
                //8-bit indexed palette
                output.Palette = palette;

                return output;
            }

            //Get the number of bytes per pixel
            switch (pfIn)
            {
                case PixelFormat.Format24bppRgb: bytesPerPixel = 3; break;
                case PixelFormat.Format32bppArgb: bytesPerPixel = 4; break;
                case PixelFormat.Format32bppRgb: bytesPerPixel = 4; break;
                default: throw new InvalidOperationException("Image format not supported");
            }

            //Lock the images
            bmpData = bmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly,
            pfIn);
            outputData = output.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.WriteOnly,
            PixelFormat.Format8bppIndexed);
            bmpStride = bmpData.Stride;
            outputStride = outputData.Stride;

            //Traverse each pixel of the image
            unsafe
            {
                byte* bmpPtr = (byte*)bmpData.Scan0.ToPointer(),
                outputPtr = (byte*)outputData.Scan0.ToPointer();

                if (bytesPerPixel == 3)
                {
                    //Convert the pixel to it's luminance using the formula:
                    // L = .299*R + .587*G + .114*B
                    //Note that ic is the input column and oc is the output column
                    for (r = 0; r < h; r++)
                        for (ic = oc = 0; oc < w; ic += 3, ++oc)
                            outputPtr[r * outputStride + oc] = (byte)(int)
                            (0.299f * bmpPtr[r * bmpStride + ic] +
                            0.587f * bmpPtr[r * bmpStride + ic + 1] +
                            0.114f * bmpPtr[r * bmpStride + ic + 2]);
                }
                else //bytesPerPixel == 4
                {
                    //Convert the pixel to it's luminance using the formula:
                    // L = alpha * (.299*R + .587*G + .114*B)
                    //Note that ic is the input column and oc is the output column
                    for (r = 0; r < h; r++)
                        for (ic = oc = 0; oc < w; ic += 4, ++oc)
                            outputPtr[r * outputStride + oc] = (byte)(int)
                            ((bmpPtr[r * bmpStride + ic] / 255.0f) *
                            (0.299f * bmpPtr[r * bmpStride + ic + 1] +
                            0.587f * bmpPtr[r * bmpStride + ic + 2] +
                            0.114f * bmpPtr[r * bmpStride + ic + 3]));
                }
            }

            //Unlock the images
            bmp.UnlockBits(bmpData);
            output.UnlockBits(outputData);

            return output;
        }

        Bitmap changePixelFormat(Bitmap b1)
        {
            

            Bitmap b2 = new

            Bitmap(b1.Size.Width, b1.Size.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            b2.SetResolution(b1.HorizontalResolution, b1.VerticalResolution);

            Graphics g = Graphics.FromImage(b2);

            g.DrawImage(b1, 0, 0);

            //continue to draw on g here to add text or graphics.

            g.Dispose();
            return b2;
        }


        Bitmap PreProcessingOnImages(Bitmap A)
        {

            A = dec_size(A);
            A = MakeGrayscale3(A);
            Bitmap B = new Bitmap(A.Width, A.Height);
            B = changePixelFormat(A);

            GaborFilter gb = new GaborFilter();

            gb.Lambda = 5;
            gb.Gamma = 0.7;
            gb.Sigma = 2.1;

            
            B = gb.Apply(B);

            return B;

        }
        private void button4_Click(object sender, EventArgs e)
        {

            Bitmap picA = new Bitmap(Aname);
            Bitmap picB = new Bitmap(Bname);
            double limit = 65000;
            if (checkBox1.Checked)
            {
                picA = PreProcessingOnImages(picA);
                picB = PreProcessingOnImages(picB);
                limit = 30000;
            }
            
            double SIMAB = SimilarityWithPCA(picA, picB);
            Console.WriteLine(SIMAB);
            if (SIMAB <= limit) {MessageBox.Show("True"); }
            else { MessageBox.Show("FALSE"); }
        }

        private void numeric_ValueChanged(object sender, EventArgs e)
        {
            PictureBoxEigenface.Image = EigenFaces[Convert.ToInt32(numeric.Value)];
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Bitmap A = new Bitmap(Aname);
            pictureBox1.Image = PreProcessingOnImages(A);
            Bitmap B = new Bitmap(Bname);
            pictureBox2.Image = PreProcessingOnImages(B);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            EigenFaces.Clear();

            // get names of all image in fa subset
            string[] allImageNames = Directory.GetFiles("JPG", "*fa*.jpg")
                                         .Select(path => Path.GetFileName(path))
                                         .ToArray();

            string[] imageNames = new string[200]; // choose a part of them
            for (int t = 0; t < 200; t++) { imageNames[t] = allImageNames[t]; }

            int imagecount = imageNames.Count();
            int imagesize = 16384; // 128 * 128
            int[,] imagesMatrix = new int[imagesize, imagecount];   // matrix of library images
            List<int[]> ImageSet = new System.Collections.Generic.List<int[]>();

            int imgid = 0;
            foreach (string fn in imageNames)
            {
                Bitmap img = new Bitmap("JPG\\" + fn);
                img = PreProcessingOnImages(img);
                int[] vec = ImgToVector(img);
                ImageSet.Add(vec);
                for (int k = 0; k < imagesize; k++)
                {
                    imagesMatrix[k, imgid] = vec[k];
                }
                imgid++;
            }

            double[,] data = matrixToDouble(transpose(imagesMatrix, new Size(imagesize, imagecount)), new Size(imagecount, imagesize));
            PrincipalComponentAnalysis pca = new PrincipalComponentAnalysis(data, AnalysisMethod.Center);
            pca.Compute();
            ArrayToImage reverse = new ArrayToImage(128, 128);


            // For each Principal Component
            for (int i = 0; i < pca.Components.Count; i++)
            {
                // We will extract its Eigenvector
                double[] vector = pca.Components[i].Eigenvector;

                // Normalize its values
                reverse.Max = vector.Max();
                reverse.Min = vector.Min();

                // Then arrange each vector value as if it was a pixel
                Bitmap eigenface; reverse.Convert(vector, out eigenface);
                eigenface = PreProcessingOnImages(eigenface);
                EigenFaces.Add(eigenface);


            }
            for (int i = 0; i < EigenFaces.Count; i++)
            {
                EigenFaces[i].Save("Eigenfaces\\EF" + i.ToString() + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
            }
        }
        
    }
}
